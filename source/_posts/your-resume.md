title: Joseba Egia Larrinaga
baseurl: "https://josebaegia.me/"
resume:
  name: JOSEBA EGIA LARRINAGA
  jobtitle: Ingeniero en Organización Industrial
  location:
    - key: city
      value: Lasarte-Oria
    - key: region
      value: Euskadi / País Vasco
    - key: countryCode
      value: ES
  contact:
    - icon: fa-external-link
      text: https://josebaegia.me
    - icon: fa-external-link
      text: https://firstcommit.dev
    - icon: fa-envelope
      text: josebaegia@protonmail.com
    - icon: fa-phone-square
      text: +34616662898
  profiles:
    - icon: fa-linkedin
      text: Linkedin
      link: https://www.linkedin.com/in/joseba-egia-larrinaga/
    - icon: fa-github
      text: GitLab
      link: https://gitlab.com/yayitazale
  overview: Soy un Ingeniero de 33 años con alta capacidad de aprendizaje y adaptación a nuevos entornos al que le gustan los retos intelectuales y los proyectos complejos. Entusiasta de la 4º revolución industrial, el IoT y los sistemas informáticos industriales, con visión de futuro sin miedo a enfrentarse a nuevas tecnologías, metodologías, lenguajes o sistemas.
  #   level: beginner, intermediate, advanced, master
  skills:
    - title: Organización Industrial
      level: avanzado
      items: [Gestión de proyectos, Procesos industriales, Gestión de almacénes, Logística, Eficiencia energética, Trazabilidad, ERP, MES, MOM]
    - title: Informática industrial
      level: avanzado
      items: [Visual Data, IIoT, Integraciones M2M, Radiofrecuencia, Redes, Ciberseguridad]
    - title: Herramientas
      level: master
      items: [Node-Red, InfluxDB, Superset, IOT2050, OPC-UA, Modbus, Profinet, SOAP, REST, Docker, ZeroTier, MQTT, JSON, ABAP ]
  experience:
    - position: MANUFACTURING ANALYTICS CONSULTANT
      duration: 2021 - Now
      company: Mondragon Sistemas de Información Grupo S. COOP.
      website: https://msigrupo.com/
      description: Ingeniero consultor en industria 4.0 para mejora de procesos productivos y eficiencia energética.
      hilights: [
        "Desarrollo e implantación de sistemas IIoT, analítica de procesos y de inteligencia artificial en la industria.",
        "Diseño de arquitecturas Edge-Cloud para captura y almacenamiento seguro de datos, implantación de sistemas Open-Source de analítica avanzada e integración con frameworks de inteligencia artificial.",
        "Definición e implantación de DSS y lazos avanzados de control inteligentes.",
        Consultoría analítica para la optimización de procesos productivos y la eficiencia energética.,
        Gestión de proyectos I+D europeos y nacionales.,
        Interconexión m2m entre sistemas de producción y explotación de datos,        
      ]
    - position: INGENIERO DE ORGANIZACIÓN INDUSTRIAL
      duration: 2016 - 2021
      company: Serkolat Bide S.L.
      website: http://www.iparlat.com/
      description: Integrado de forma transversal en las 3 áreas del departamento informático del grupo empresarial Iparlat/Kaiku; sistemas informáticos industriales, desarrollo ERP y sistemas e infraestructuras informáticas. 
      hilights: [
        "Implantación de la plataforma de industria 4.0 MES-MOM Simatic IT de Siemens en varias plantas del grupo. Definición del proyecto, planificación, presupuestos, elección de elementos tecnológicos, integración con el ERP, definición de operativa de usuario y front-end, integración con sistemas de visualización de Big-Data en tiempo real, configuración e implantación de equipos informáticos industriales y formación a operarios y Key Users.",
        "Soporte técnico a todos los almacenes con trazabilidad de producto terminado del grupo en España y Chile; Sistemas industriales de etiquetado de palets, equipos de radiofrecuencia, almacenes robotizados y módulos logístico y Warehouse Management (WM) en SAP R/3.",
        "Programación del ERP SAP R/3 a medida en los módulos de producción, WM, compras, logística, financiero, etc. e integración con sistemas informáticos externos mediante webservice y RFC.",
        "Definición, implantación y gestión de sistemas informáticos como; ciberseguridad Palo Alto CORTEX XDR, monitorización en tiempo real de redes e inventario de red informática en tiempo real con Lansweeper."
      ]
    - position: TÉCNICO DE LABORATORIO TECNOLÓGICO
      duration: 2013 - 2016
      company: CICC Tabakalera SA
      website: https://www.tabakalera.eus/es/medialab
      description: Técnico ingeniero en el laboratorio tecnológico Hirikilabs del CICC Tabakalera. Desarrollo técnico completo del proyecto tanto de divulgación de la ciencia y tecnología como servicio a artistas.
      hilights: [
        "Definición inicial de maquinaria necesaria (impresión 3D, mecanizado CNC y corte láser, soldadura, estaciones de electrónica, diseño 2D/3D, etc).",
        "Definición de necesidades constructivas del espacio y layout, gestión de licitaciones públicas y compras de material tecnológico, puesta en marcha y mantenimiento nimiento del equipamiento.",
        "Formación tecnológica a usuarios y artistas.",
        Desarrollo e implantación de numerosos sistemas tecnológicos en exposición."
      ]
    - position: TÉCNICO DE TRAZABILIDAD
      duration: 2012
      company: Serkolat Bide S.L.
      website: http://www.iparlat.com/
      description: Técnico en área de trazabilidad en sustitución durante 4 meses dando soporte a todo el grupo Iparlat - Kaiku en España y Chile.
      hilights: [
        "Soporte telemático en SAP WM R/3.",
        "Gestión de almacenes robotizados, informática industrial, radiofrecuencia y etiquetado industrial.",
        "Programación del ERP SAP R/3 a medida en los módulos de producción, WM, compras, logística, financiero, etc. e integración con sistemas informáticos externos mediante webservice y RFC."
      ]
    - position: SOCIO
      duration: 2009 - 2015
      company: Dinitek Junior Empresa
      description: "Miembro de la Junior Empresa Dinitek de la Escuela de Ingeniería de Gipuzkoa. Distintos cargos durante los años como socio: RRHH, jefe del Dpt. Informática, vicepresidente y Presidente."
      hilights: [
        "Gestión de proyectos de desarrollo de soluciones tecnológicas a medida para varios clientes mediante prototipado 3D, mecanizado y electrónica",
        "Gestión de proyectos de fin de carrera de alumnos con participación en concursos de robótica y MotoStudent.",
        "Desarrollo de concursos técnicos a nivel de UPV/EHU"
      ]
    - position: DEPENDIENTE VOLUNTARIO
      duration: 2012
      company: Fundación Intermon Oxfam Irlanda
      website: https://www.oxfamireland.org/
      description: Catalogado y venta a cliente en tienda de objetos de segunda mano como electrodomésticos, muebles, discos, libros, etc. durante 4 meses en Dublín.
    - position: CAMARERO
      duration: 2008 - 2010
      company: Bar La pluma
      description: Camarero de barra y terraza.
  
      # Education
  education:
    - institution: Escuela de Ingeniería de Gipuzkoa
      course: INGENIERÍA EN ORGANIZACIÓN INDUSTRIAL
      duration: 2014
      location: Donostia / San Sebastían, ES
    - institution: Ikergune
      course: PROGRAMMING FOR ROBOTICS - ROS
      duration: 2014
    - institution: Eden School Dublin
      course: INGLES PREPARATORIO C1
      duration: 2011
      location: Dublin, IE
    - institution: Escuela de Ingeniería de Gipuzkoa
      course: INGENIERÍA TEC. INDUSTRIAL ESP. EN ELECTRÓNICA INDUSTRIAL
      duration: 2010
      location: Donostia / San Sebastían, ES
    - institution: Ibermática
      course: INICIACIÓN A SOLIDWORKS
      duration: 2009
      location: Donostia / San Sebastían, ES
    - institution: Dep. Educación Gobierno Vasco
      course: TITULO EGA
      duration: 2009
      location: Donostia / San Sebastían, ES

      
# Showcase your work
  projects:
  - title: firstcommit.dev
    description: Blog personal de dos amgios sobre proyectos domesticos relacionados con el IoT, la domótica y el DIY.
    duration: 2021
    contribution: Co-creador.
  - title: Landazale
    description: Creación de una start-up para poner en marcha un sistema logístico sostenible de compra de frutas y verduras km0, transporte en vehículos eléctricos y entrega/recogida en taquillas IoT conectadas a Internet.
    duration: 2013
    contribution: Co-Fundador.
  - title: Ikasle Poliedrikoak
    description: Creación de asociación universitaria para la representación del alumnado en elecciones al los consejos de estudiantes de la Escuela de Ingeniería de Gipuzkoa. Participación en órganos de representación a nivel de la escuela, campus de gipuzkoa y la UPV/EHU.
    duration: 2012
    contribution: Fundador.
# Langguages Known 
# proficiency: beginner, intermediate, advanced, master, fluent, native speaker
  languages:
  - name: Euskera
    proficiency: nativo
  - name: Castellano
    proficiency: nativo
  - name: English
    proficiency: fluent
